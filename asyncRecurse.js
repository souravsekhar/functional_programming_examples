// function that makes asynchronous calls synchronous

function asyncSerial() {	
  var args = Array.prototype.slice.apply(arguments);	  
  function next() {
    var func = args.shift();
    if (func) {
      func(next);
    }
  }
  next();
}

// asynchronous calls to be called synchronously

function async1 (next) {
	setTimeout(function (){			
		next();
	}, 1000);
}

function async2 (next) {
	setTimeout(function (){			
		next();
	}, 5000);
}

function async3 (next) {
	setTimeout(function (){			
		next();
	}, 2000);
}

asyncSerial(async1, async2, async3);