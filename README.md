# Functional Programming #

These examples would help you kickstart functional programming.

### Concepts to strive for ###

* Declarative coding
* Higher-order functions
* Pure functions
* Stateless programming
* Avoid side-effects
* Currying
* Function composition
* Memoisation
* Recursion
* Functors
* Monads

### Some useful links to start with ###

* https://medium.com/javascript-scene/master-the-javascript-interview-what-is-functional-programming-7f218c68b3a0

### Who do I talk to in case of doubts? ###

* Google
* Your inner soul ;)